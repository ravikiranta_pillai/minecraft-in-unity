﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World:MonoBehaviour {

	public Biome[] biomes;
	public static World currentWorld;
	public int chunkWidth = 20,chunkHeight = 20,seed = 0;
	public float viewRange = 30;
	public Transform parent;
	public Chunk chunkPrefab;
	bool worldGenCompleted;

	void Awake() {
		currentWorld = this;
		if(seed == 0){
			seed = Random.Range (0, int.MaxValue);
		}
		worldGenCompleted = false;
	}

	void Start() {
		StartCoroutine (UpdateWorld ());
	}

	IEnumerator UpdateWorld() {
		while (true) {
			for (float x = transform.position.x - viewRange; x <= transform.position.x + viewRange; x += chunkWidth) {
				for (float z = transform.position.z - viewRange; z <= transform.position.z + viewRange; z += chunkWidth) {
					Vector3 pos = new Vector3 (x, 0, z);
					pos.x = Mathf.Floor (pos.x / (float)chunkWidth) * chunkWidth;
					pos.z = Mathf.Floor (pos.z / (float)chunkWidth) * chunkWidth;

					Chunk chunk = Chunk.FindChunk (pos);

					if (chunk == null) {
						chunk = (Chunk)Instantiate (chunkPrefab, pos, Quaternion.identity,parent);
					}
					yield return new WaitForSeconds (1/10);
				}
			}
			worldGenCompleted = true;
			transform.parent.GetComponent <Rigidbody>().useGravity = true;
		}
	}
}

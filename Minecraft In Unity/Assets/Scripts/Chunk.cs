﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimplexNoise;

[System.Serializable]
public enum BlockType {
	None,

	PowderStone,
	SmoothStone,
	Dirt,
	GrassSide,
	WoodPlank,
	CutStoneSide,
	CutStoneTop,
	Bricks,
	TNTSide,
	TNTTop,
	TNTBottom,
	Web,
	RedFlower,
	YellowFlower,
	DeepWater,
	Sapling,
	CobbleStone,
	Granite,
	Sand,
	Gravel,
	WoodBlockSide,
	WoodBlockTop,
	IronBlock,
	GoldBlock,
	DiamondBlock,
	SingleBoxTop,
	SingleBoxSide,
	SingleBoxFront,
	MushroomRed,
	MushroomBrown,
	Gold,
	Iron,
	Coal,
	Library,
	GreenStone,
	Obsidian,
	BlackSide,
	BlackTop,
	Snow = 67,
	Ice,
	GrassTop = 205,
	Water,
	Lava = 256
}

[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshCollider))]
[RequireComponent (typeof(MeshRenderer))]
public class Chunk : MonoBehaviour {
	#region Variables
	public byte[,,] map;
	public static List<Chunk> chunks = new List<Chunk>();

	public static int Width{
		get{
			return World.currentWorld.chunkWidth;
		}
	}

	public static int Height{
		get{
			return World.currentWorld.chunkHeight;
		}
	}

	protected Mesh visualMesh;
	protected MeshCollider meshCollider;
	protected MeshFilter meshFilter;
	#endregion

	#region Init
	void Start () {
		chunks.Add (this);
		meshFilter = GetComponent <MeshFilter> ();
		meshCollider = GetComponent <MeshCollider> ();

		CalculateMapFromScratch ();
		//StartCoroutine (CalculateMapFromScratch());
		//StartCoroutine (CreateVisualMesh());
	}

	public static byte GettTheoreticalByte(Vector3 pos) {
		Random.seed = World.currentWorld.seed;
		Vector3 grain0Offset = new Vector3 (Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain1Offset = new Vector3 (Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain2Offset = new Vector3 (Random.value * 10000, Random.value * 10000, Random.value * 10000);

		return GetTheoreticalByte (pos, grain0Offset, grain1Offset, grain2Offset);
	}

	public static byte GetTheoreticalByte(Vector3 pos,Vector3 offset0,Vector3 offset1,Vector3 offset2) {
		

		float clusterValue = CalculateNoiseValue (pos, offset2, 0.002f);
		int biomeIndex = Mathf.FloorToInt (clusterValue * World.currentWorld.biomes.Length);
		Biome biome = World.currentWorld.biomes [biomeIndex];

		float heightBase = biome.minHeight;
		float maxHeight = biome.maxHeight;
		float heightSwing = maxHeight - heightBase;

		float blobValue = CalculateNoiseValue (pos,offset1, 0.05f);
		float mountainValue = CalculateNoiseValue (pos, offset0, 0.009f);

		mountainValue += biome.mountainPowerBonus;
		if (mountainValue < 0)
			mountainValue = 0;

		mountainValue = Mathf.Pow (mountainValue, biome.mountainPower);

		byte block = biome.GetBrick (Mathf.FloorToInt (pos.y),mountainValue,blobValue);

		mountainValue *= heightSwing;
		mountainValue += heightBase;
		mountainValue += blobValue * 10 - 5f;

		//--- Map values ---
		if (mountainValue >= pos.y)
			return block;
		else
			return 0;
	}

	public void CalculateMapFromScratch() {
		map = new byte[Width,Height,Width];
		Random.seed = World.currentWorld.seed;
		Vector3 grain0Offset = new Vector3 (Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain1Offset = new Vector3 (Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain2Offset = new Vector3 (Random.value * 10000, Random.value * 10000, Random.value * 10000);


		for(int x = 0; x <Width; x++) {
			for (int y = 0; y < Height; y++) {
				for (int z = 0; z < Width; z++) {
					map [x, y, z] = GetTheoreticalByte (new Vector3 (x,y,z) + transform.position,grain0Offset,grain1Offset,grain2Offset);
				}
			}
		}
		StartCoroutine (CreateVisualMesh());
	}

	public static float CalculateNoiseValue(Vector3 pos,Vector3 offset, float scale){
		float noiseX = Mathf.Abs(pos.x + offset.x) * scale;
		float noiseY = Mathf.Abs(pos.y + offset.y) * scale;
		float noiseZ = Mathf.Abs(pos.z + offset.z) * scale;

		return Mathf.Max (0,Noise.Generate (noiseX, noiseY, noiseZ));
	}

	public virtual IEnumerator CreateVisualMesh() {
		visualMesh = new Mesh ();
		List<Vector3> verts = new List<Vector3> ();
		List<int> tris = new List<int> ();
		List<Vector2> uvs = new List<Vector2>();

		for (int x = 0; x < Width; x++) {
			for (int y = 0; y < Height; y++) {
				for (int z = 0; z < Width; z++) {
					
					if (map [x, y, z] == 0)
						continue;
					DrawBrick (x,y,z,map[x,y,z],verts,tris,uvs);
				}
			}
		}

		visualMesh.vertices = verts.ToArray ();
		visualMesh.triangles = tris.ToArray ();
		visualMesh.uv = uvs.ToArray ();

		visualMesh.RecalculateBounds ();
		visualMesh.RecalculateNormals ();

		meshFilter.mesh = visualMesh;
		meshCollider.sharedMesh = visualMesh;

		yield return new WaitForSeconds ((1/10));
	}
	
	public void DrawBrick(int x,int y, int z, byte block,List<Vector3> verts,List<int> tris,List<Vector2> uvs) {
		Vector3 start = new Vector3 (x, y, z);
		Vector3 offset1, offset2;

		//Left Face
		if(IsTransparent (x - 1,y,z)){
			offset1 = Vector3.up;
			offset2 = Vector3.back;
			BuildFace (start, offset1, offset2, block,verts,tris,uvs);
		}
		//Right Face
		if(IsTransparent (x + 1,y,z)){
			offset1 = Vector3.down;
			offset2 = Vector3.back;
			BuildFace (start + Vector3.right + Vector3.up, offset1, offset2, block,verts,tris,uvs);
		}
		//Bottom Face
		if(IsTransparent (x,y - 1,z)){
			offset1 = Vector3.left;
			offset2 = Vector3.back;
			BuildFace (start + Vector3.right, offset1, offset2, block,verts,tris,uvs);
		}
		//Top Face
		if(IsTransparent (x,y + 1,z)){
			offset1 = Vector3.right;
			offset2 = Vector3.back;
			BuildFace (start + Vector3.up, offset1, offset2, block,verts,tris,uvs);
		}

		//Forward Face
		if(IsTransparent (x,y,z - 1)){
			offset1 = Vector3.up;
			offset2 = Vector3.right;
			BuildFace (start + Vector3.back, offset1, offset2, block,verts,tris,uvs);
		}

		//Back Face
		if(IsTransparent (x,y,z + 1)){
			offset1 = Vector3.right;
			offset2 = Vector3.up;
			BuildFace (start, offset1, offset2, block,verts,tris,uvs);
		}
	}

	public void BuildFace(Vector3 corner,Vector3 up,Vector3 right, byte block,List<Vector3> verts,List<int> tris,List<Vector2> uvs){
		int index = verts.Count;

		verts.Add (corner);
		verts.Add (corner + up);
		verts.Add (corner + right);
		verts.Add (corner + up + right);

		Vector2 uvWidth = new Vector2 (0.0625f, 0.0625f);

		//---- Texture is a 16 X 16 block atlas --- So convert on x and y axis from 1D block to 2D block texture coordinates  --
		Vector2 uvCorner = new Vector2 ((block % 16 - 1) * uvWidth.x ,1 - (uvWidth.y * ( 1 + Mathf.Floor(block / 16))));

		uvs.Add (uvCorner);
		uvs.Add (new Vector2(uvCorner.x,uvCorner.y + uvWidth.y));
		uvs.Add (new Vector2(uvCorner.x + uvWidth.x,uvCorner.y));
		uvs.Add (new Vector2(uvCorner.x + uvWidth.x,uvCorner.y + uvWidth.y));

		tris.Add (index + 0);
		tris.Add (index + 1);
		tris.Add (index + 2);
		tris.Add (index + 3);
		tris.Add (index + 2);
		tris.Add (index + 1);
	}
	#endregion

	#region InternalFunctions
	public bool IsTransparent(int x, int y, int z){
		byte brick = GetByte (x,y,z);

		if (y < 0)
			return false;
		
		switch(brick) {
		case 0:
			return true;
			break;
		default:
			return false;
			break;
		}
	}

	public virtual byte GetByte(int x, int y, int z){
		if (y < 0 || y >= Height)
			return 0;

		if ((x < 0) || (z < 0) || (x >= Width) || (z >= Width)) {
			Vector3 worldPos = new Vector3 (x, y, z) + transform.position;
			Chunk chunk = Chunk.FindChunk (worldPos);
			if (chunk == this) return 0;
			if (chunk == null)
				return GettTheoreticalByte (worldPos);

			return chunk.GetByte (worldPos);
		}
		return map[x,y,z];
	}

	public virtual byte GetByte(Vector3 worldPos) {
		worldPos -= transform.position;
		int x = Mathf.FloorToInt ((worldPos.x));
		int y = Mathf.FloorToInt ((worldPos.y));
		int z = Mathf.FloorToInt ((worldPos.z));
		return GetByte (x, y, z);
	}

	public static Chunk FindChunk(Vector3 pos) {
		for(int i = 0 ; i < chunks.Count; i++) {
			Vector3 cpos = chunks [i].transform.position;

			if(pos.x < cpos.x || pos.z < cpos.z || pos.x >= cpos.x + Width || pos.z >= cpos.z + Width)continue;
			return chunks [i];
		}
		return null;
	}
	#endregion

	#region ChunkModificationFunctions
	public bool SetBlock(byte block, Vector3 worldPos){
		worldPos -= transform.position;
		return SetBlock (block, Mathf.FloorToInt (worldPos.x), Mathf.FloorToInt (worldPos.y), Mathf.FloorToInt (worldPos.z));
	}

	public bool SetBlock(byte block, int x, int y, int z){
		if((x < 0 || (y < 0) || (z < 0) || (x >= Width) || y >= Height)|| (z >= Width)){
			return false;
		}
		if (map [x, y, z] == block) return false;

		map [x, y, z] = block;

		StartCoroutine (CreateVisualMesh ());

		//---- In case of chunk edge blocks --- Update next check edge as well --
		if(x == 0){
			Chunk chunk = FindChunk (new Vector3 (x - 1, y, z) + transform.position);
			if (chunk != null)
				StartCoroutine (chunk.CreateVisualMesh ());
		}
		else if(x == Width - 1){
			Chunk chunk = FindChunk (new Vector3 (x + 1, y, z) + transform.position);
			if (chunk != null)
				StartCoroutine (chunk.CreateVisualMesh ());
		}
		if(z == 0){
			Chunk chunk = FindChunk (new Vector3 (x, y, z - 1) + transform.position);
			if (chunk != null)
				StartCoroutine (chunk.CreateVisualMesh ());
		}
		else if(z == Width - 1){
			Chunk chunk = FindChunk (new Vector3 (x, y, z + 1) + transform.position);
			if (chunk != null)
				StartCoroutine (chunk.CreateVisualMesh ());
		}
		return true;
	}
	#endregion
}

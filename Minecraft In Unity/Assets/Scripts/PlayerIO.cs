﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIO : MonoBehaviour {

	public static PlayerIO currentPlayerIO;
	public float maxInteractionRange;
	public byte selectedInventory = 0;

	private Camera cam;
	private GameObject debugSphere;

	void Start () {
		currentPlayerIO = this;
		cam = GetComponent <Camera> ();
		//debugSphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
	}
	
	void Update () {
		// Left click
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = cam.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0.5f));
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, maxInteractionRange)) {
				//Debug.Log (hit.point + " " + hit.transform.name);
				Chunk chunk = hit.transform.GetComponent <Chunk> ();
				if (chunk == null) {
					Debug.Log ("Got hit but couldnt get a chunk at this position");
					return;
				}

				Vector3 p = hit.point;
				p -= hit.normal / 4;
				p.x = Mathf.Floor (p.x);
				p.y = Mathf.Floor (p.y);
				p.z = Mathf.Floor (p.z) + 1;

				selectedInventory = chunk.GetByte (p);
				//Instantiate (debugSphere,p,Quaternion.identity);
				//Debug.Log (p + " " + hit.transform.name);

				chunk.SetBlock ((byte)0, new Vector3 (p.x, p.y, p.z));
			}
		}
		else if (Input.GetMouseButtonDown (1)) {
			Ray ray = cam.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0.5f));
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, maxInteractionRange)) {
				Chunk chunk = hit.transform.GetComponent <Chunk> ();
				if (chunk == null) {
					Debug.Log ("Got hit but couldnt get a chunk at this position");
					return;
				}

				Vector3 p = hit.point;
				if (selectedInventory != 0) {
					p += hit.normal / 4;
					p.x = Mathf.Floor (p.x);
					p.y = Mathf.Floor (p.y);
					p.z = Mathf.Floor (p.z) + 1;
					chunk.SetBlock ((byte)selectedInventory, new Vector3 (p.x, p.y, p.z));
				}
			}
		}
	}
}

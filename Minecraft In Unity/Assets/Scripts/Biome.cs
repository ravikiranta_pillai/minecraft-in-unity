﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class Biome{
	public string name = "Unknown Biome";
	[Multiline]public string desc;
	public float mountainPower = 1;
	public float minHeight = 10,maxHeight = 10;
	public float mountainPowerBonus = 0;
	public BlockLayer[] blockLayers;

	public byte GetBrick(int y, float mountainValue, float blobValue) {
		BlockLayer bestBidder = null;

		float bestBid = 0;;
		for(int i = 0; i< blockLayers.Length; i++) {
			float bid = blockLayers [i].Bid (y, mountainValue, blobValue);
			if(bid > bestBid){
				bestBid = bid;
				bestBidder = blockLayers [i];
			}
		}

		if (bestBid == null)
			return 0;
		else
			return (byte)bestBidder.block;
	}
}

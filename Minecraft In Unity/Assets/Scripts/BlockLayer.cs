﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockLayerCondition {
	None,
	AboveGroundLevel,
	GroundLevel,
	BelowGroundLevel
}

[System.Serializable]
public class BlockLayer {
	public string name = "UnnamedBrickLayer";
	public BlockType block;
	public float weight = 1;
	public BlockLayerCondition[] conditions;

	public virtual float Bid(int y, float mountainValue, float blobValue) {
		float bid = 0;

		for(int i = 0; i < conditions.Length; i++) {
			switch(conditions[i]){
			case BlockLayerCondition.None:{
					bid ++;
					break;
				}
			case BlockLayerCondition.AboveGroundLevel:{
					if (y > 10)	bid++;
					break;
				}
			case BlockLayerCondition.BelowGroundLevel:{
					if (y < 10)	bid++;
					break;
				}
			case BlockLayerCondition.GroundLevel:{
					if (y > 0 && y < 12) bid++;
					break;
				}
			}
		}

		if (weight == 0) return bid;
		return bid * weight;


	}
}
